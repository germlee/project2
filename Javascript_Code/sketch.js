/*
	Project 2
	Name of Project: typing game
	Author: Giyun Lee
	Date:
*/

//game setting
var gs =
{
	settingTitle : "",

	wb : [],
	wb2 : [],
	wb3 : [],
	wbh : [],
	
	appearanceDelay : 0, //period
	monsterSpeed : 0,
	numOfMonsters : 0, // on each rail
	numOfHearts : 0
}

let params = {

	interval : 5,  //5s->5000milisec
	intervalMin : 2,
	intervalMax : 9,
	intervalStep : 1,

	monsterSpeed : 0.5,
	monsterSpeedMin : 0.2,
	monsterSpeedMax : 0.9,
	monsterSpeedStep : 0.1,

	numOfMonsters : 30,
	numOfMonstersMin : 18,
	numOfMonstersMax : 60,
	numOfMonstersStep : 3,

	numOfHearts : 3,
	numOfHeartsMin : 0,
	numOfHeartsMax : 5,
	numOfHeartsStep : 1

}

let defaultSetting =
{
	settingTitle : "default setting",
	
	wb : "hello world coffee flavor wheel key board cat phone ice mouse shoes laptop juice beer beef teapot sugar chair fish paper print taxi car cow sofa dog cat ship ghost wolf timer",
    wb2 : "tissue computer product function abstract glasses doorbell factory pistol rifle interface favorite fresh monster zombie window error extend wizard kaist harmony thread google drive",
    wb3 : "processing software parameter television crosswalk communicate condition implement arraylist polymorphism javascript reference development prototyping deadline objective encourage library inheritance different repository challenge summary document",
    wbh : "hello world coffee flavor wheel key board cat phone ice mouse shoes laptop juice beer beef teapot sugar chair fish paper print taxi car cow sofa dog cat ship ghost wolf timer tissue computer product function abstract glasses doorbell factory pistol rifle interface favorite fresh monster zombie window error extend wizard kaist harmony thread google drive processing software parameter television crosswalk communicate condition implement arraylist polymorphism javascript reference development prototyping deadline objective encourage library inheritance different repository challenge summary document",
	
	appearanceDelay : 5000,
	monsterSpeed : 0.5,
	numOfMonsters : 10,
	numOfHearts : 3
}

let title;
let div, div1, div2, div3, div4;
let words;
let button1, button2, button3, button4;
let prev;

let gui;
let json;
let msg;
let img;

function preload(){
	img = loadImage('image.png');
}

function setup (){

	createCanvas(1000, 1000);

	div = createDiv('Game Setting for Typing Game');
	div.position(250,20);
	div.style('font-size','37px');
	div.style('font-weight','bold');
	div.style('color',color(255,241,228));

	//instructions
	div1 = createDiv('Enter a title of the game setting.')
	div1.position(250,115);
	div1.style('font-size','27px');
	div1.style('color',color(255,241,228));

	// typing setting title
	title = createInput('setting title');
	title.position(250,170);
	title.size(300,40);
	title.style('font-size','26px');

	//instructions 1
	div2 = createDiv('Enter the words or text you want to practice in the game.')
	div2.position(250,250);
	div2.style('font-size','27px');
	div2.style('color',color(255,241,228));

	//typing words
	words = createInput('type any words you want');
	words.position(250,300);
	words.size(700,40);
	words.style('font-size','26px');
	
	//preview button
	button1 = createButton('preview');
	button1.position(250,400);
	button1.size(130,50);
	button1.style('font-size','30px');
	button1.style('color',color(255));
	button1.style('background-color',color(10,41,59));
	button1.mousePressed(preview);

	//submit button
	button2 = createButton('submit');
	button2.position(400,400);
	button2.size(120,50);
	button2.style('font-size','30px')
	button2.style('color',color(255));
	button2.style('background-color',color(10,41,59));
	button2.mousePressed(submit);

	//ranking button
	button3 = createButton('check ranking');
	button3.position(20,400);
	button3.size(200,50);
	button3.style('font-size','28px')
	button3.style('color',color(255));
	button3.style('background-color',color(10,41,59));
	button3.mousePressed(getRankingInfo);

	//initialization
	button4 = createButton('initialization');
	button4.position(540,400);
	button4.size(180,50);
	button4.style('font-size','28px')
	button4.style('color',color(255));
	button4.style('background-color',color(10,41,59));
	button4.mousePressed(initialization);
	
	// create GUI
	gui = createGui("edit game setting");
	gui.addObject(params);

	let instruction = 
	"You can edit the game setting in this page."
	+ "<br/>Enter a title of the new game setting."
	+ " And then, enter any words or text you want.<br/>This generates words that you want to practice in the typing game."
	+ "<br/><br/>Click the preview button before submit."
	+ "<br/>Then you can see what you are going to submit."
	+ "<br/>If you are satisfied with the setting, then press the submit button."
	+ "<br/>If you want to edit the setting, then rewrite the title and words and modulate using GUI."
	+ "<br/><br/>If you want to know the best score, click the check ranking button."
	+ "<br/>Then the best scores in each level is showed."
	
	div3 = createDiv('**** instructions ****');
	div3.position(20,470);
	div3.style('font-size','30px');
	div3.style('font-weight','bold');
	div3.style('color',color(255,241,228));

	div4 = createDiv(instruction);
	div4.position(20,510);
	div4.style('font-size','24px');
	div4.style('color',color(255,241,228));

	noLoop();
}

function draw(){
	
	background(152,110,70);
	image(img,30,840,950,142);
}

// <WORD POOL>
// <EDIT GAME LEVEL>
let again = false;
let count = 0;
function preview()
{	// if press submit again, wb, wb2, wb3 must be refreshed
	
	if(again)
	{
		gs.wb = []; 
		gs.wb2 = []; 
		gs.wb3 = []; 
		gs.wbh = []; 
	}

	const text = words.value();

	let splitText = splitTokens(text,'-(),.?!;\" ');
	
	for(let w of splitText)
	{
		w = w[0].toLowerCase() + w.substring(1);
		if(w.length>3 && !w.includes("\'"))
		{
			if(w.length<6) gs.wb.push(w);
			else if(w.length<9) gs.wb2.push(w);
			else gs.wb3.push(w);
		}
	}

	gs.wb = checkOverlap(gs.wb); 
	gs.wb2 = checkOverlap(gs.wb2); 
	gs.wb3 = checkOverlap(gs.wb3); 
	gs.wbh = gs.wb.concat(gs.wb2).concat(gs.wb3); 

	// save setting title
	gs.settingTitle = title.value();
	print("preview title");

	// save others
	gs.appearanceDelay = params.interval*1000;
	gs.monsterSpeed = params.monsterSpeed;
	gs.numOfMonsters = params.numOfMonsters/3;
	gs.numOfHearts = params.numOfHearts;

	words.value('');
	title.value('');
	
	let str = "setting title : " + gs.settingTitle
	+ "<br/>words for monster lv1 : " + gs.wb.join(' ')
	+ "<br/>words for monster lv2 : " + gs.wb2.join(' ')
	+ "<br/>words for monster lv3 : " + gs.wb3.join(' ')
	+ "<br/>interval(s) : " + gs.appearanceDelay
	+ "<br/>monster speed : " + gs.monsterSpeed
	+ "<br/>the number of monsters : " + gs.numOfMonsters*3
	+ "<br/>the number of hearts : " + gs.numOfHearts;
	
	prev = createP(str);
	prev.style('font-size','25px');

	again = true;
	count++;
}

function submit()
{	
	if(count == 0) preview();

	//print(gs.settingTitle);
	// array into string
	let gscopy = JSON.parse(JSON.stringify(gs));
	gscopy.wb = gs.wb.join(' ');
	gscopy.wb2 = gs.wb2.join(' ');
	gscopy.wb3 = gs.wb3.join(' ');
	gscopy.wbh = gs.wbh.join(' ');

	// send info to server
	console.log(JSON.stringify(gscopy));
	let msg = JSON.stringify(gscopy);
	ws.send(msg);
}

function checkOverlap(arr)
{
	let a = [];
	let check = false;

	for(let i=0;i<arr.length;i++)
	{
		for(let j=i+1;j<arr.length;j++)
		{
			if(arr[i]===arr[j]) 
			{
				check = true;
				break;
			}
		}
		if(!check) a.push(arr[i]);
		check = false;
	}
	return a.slice();
}

function getRankingInfo()
{
	ws.send("ranking");
}

// <RANKING>
function ranking(message)
{
	let rank = JSON.parse(message);

	let currentSetting = rank.records.filter(e =>
		e.settingTitle == rank.currentSetting);
	
	let winner = currentSetting.filter(e => e.win == "win");

	let level = winner.map(e => e.level);
	level = checkOverlap(level);

	let bestRecords = level.map(e =>
		{
			let lv = winner.filter(x => x.level === e);
		
			let best;
			for(let i=0; i<Object.keys(lv).length-1;i++)
			{
				if(lv[i].score>lv[i+1].score) best = lv[i];
				else if(lv[i].score<lv[i+1].score) best = lv[i+1];
				else if(lv[i].score==lv[i+1].score)
				{
					if(lv[i].playtime<=lv[i+1].playtime) best = lv[i];
					else best = lv[i+1];
				}
			}
			return best;
		});
	
	//return bestRecords;
	let showRanking = bestRecords.reduce((acc,e) => {
		let str = "level : " + e.level
		+ "<br/>score : " + e.score
		+ " playtime : " + e.playtime + "<br/>";
		acc+=str;
		print(acc);
		return acc;
	},"");

	let finalstr = "Ranking<br/>" + showRanking;

	let r = createP(finalstr);
  	r.style('font-size','27px');
}

function initialization()
{
	ws.send(JSON.stringify(defaultSetting));
}

var ws = new WebSocket("ws://localhost:8080/typingGame");

$(function (){
	ws.onopen = function(evt) {
		console.log("Ready...");
	};

	ws.onmessage = function (evt) {
		var received_msg = evt.data;
		console.log("Message is received..." + received_msg);

		ranking(received_msg);
	};

	ws.onclose = function() {
		console.log("Connection is closed...");
	};
});
