# Project 1 template #

This is the template for your project 1 submission. For a full description of the Project, please refer to [this document](https://docs.google.com/document/d/1WhCeZpnAJyuraJI2cAsU8thabzHej7SHa_2b4pQv9Ls/edit?usp=sharing).

### Table of content

* [Source code](Project1_Code/)
* [Demo video](Video/)
* Description of projects and notes in README.md (this file). 
	
	

### Description of the project

Please modify this file to include a description of your project.
You can add images here, as well as links. 
To format this document you will need to use Markdown language. Here a [tutorial](https://bitbucket.org/tutorials/markdowndemo).

Include the following things inside this file:

* briefly describe your project (objectives and what it does)
* describe how to use your project.\
* list dependencies, if any (e.g., libraries you used)
* state sources of inspiration/code that you used or if anyone helped you


## How to use my project

My project is a typing game. It is a game that kills and get score by typing the words that appear with the monsters that appear on the right.

Initially, you can choose a game level. The higher the game level, the more monsters in the higher level appear.

There are three types of monsters depending on their level. Monsters have different types of words(differ by difficulty to type), speed, and assigned scores depending on their level. Low-level monsters have easy words and slowly move. On the other hand, high-level monsters have difficult words and are fast-paced. The higher the level, the more points you get when you kill the monster.

You can check the word input at the bottom center. Type in and hit enter or spacebar to enter the word. It can be erased using backspace.
Before the monster reaches the player on the left, you must enter the word and kill it. If you fail to enter the word and the monster reaches the player, the heart is reduced. When all the hearts disappear and are attacked once more, the game is over.

Hearts may appear instead of monsters on the right. If you enter the word that the heart has, the number of hearts increases.

You can see the current score and the number of hearts at the top left.

When the game is over, the score is displayed with the phrase that it is over.


## brief description of my project
I wrote project 1 and an additional page named 'additional code'.

### additional_code

#### abstract class target
  
  The **target** class is an abstract class and is the parent of monster and heart.

  name is the word the target will have.

  picture is the image the target will have.

  The constructor takes the row and col as parameters and stores them in x, y as a location.

  As abstract method, it has namelist and draw, namelist is a method that extracts words from a file and puts them in name, and draw is a method that determines how it looks on the screen.

  The checking method is a method that compares the word entered by the player with the target name and returns a boolean.
  
#### class monster extends target
  
  monster is a child class of target.'

  level is taken as a parameter in the constructor and stored.

  namelist method was overridden to extract words from different files according to the level.

  draw method also shows different images depending on the level and the name. The movement speed was also set differently.

#### class heart extends target
  
  The heart class is a child class of target.
  
  Like the monster class, I override the namelist method and draw method.

#### class popMonster extends Thread
  
  popmonster class extends Thread to create monsters at regular intervals.

  constructor takes delay and col as parameters. delay has been added to vary the timing of starting. col is a value to determine the rail on which the monster appears on the screen.

  run method creates a monster that is assigned according to the game level (allocateByLevelCopy). Create 10 times. The created monster is put in showedMonsters. showedMonsters is an arraylist of monsters to show on the screen.

#### class popHeart extends Thread
  
  popHeart has a structure similar to popMonster. However, popHeart has no level.

#### webSocketServerEvent
  This method is to receive and send messages with the javascript client.
  If it get a sign "ranking", then it send back the information including the title of the current setting and records.
  Also if it get other message, then it pares the json string into json object, and save it in 'gamesetting.json'.

### Project1_Code

#### void keyTyped
  
  keyTyped is a method for storing the character (whatUserTyping) the player is currently entering.
  
  If you press space and enter, it is saved in whatUserTyped and can kill monsters.

  You can erase text with backspace.


#### void showscore
  
  This method shows the current score on the screen.

#### void showtyping
  
  This method is for showing the character currently being input.
  
  If the entered characters are longer than the text box, only the back is displayed to fit the box size.
  
#### void drawtypingbox
  
  This is a method for drawing a text box.

#### void drawplayer

This is a method for drawing player image, checking the end of the game.
When the game is end, it check the time and calculate the playing time. Then save information including the title of the current game setting, whether you win or not, the level you played, and the playing time in the json file named 'ranking.json'.

#### void drawmonster
  
  drawmonster is a method for showing monsters on the screen.

  All monsters in showedMonstersCopy are displayed on the screen. If the word entered by the player matches the name of a certain monster, a score is given, and the entered word is initialized and the monster is removed from showedMonsters (array list of monsters to be shown on the screen).
  
  Also, if the monster comes too close to the left, the score is cut and the monster is removed. It also gives effect sound.
  

#### void drawheart
  
  draw heart is a method for showing the heart on the screen.

  If the heart is gone, stop showing the monster and show the phrase 'game over' and the current score.

  When the game end, it do the same roles that method dreawplayer do. It calculate the playing time, and save the records in 'ranking.json' file.

#### void drawSmallHearts
  
  drawSmallHearts is a method for showing the stored hearts on the screen.

#### void allocateByLevel
  
  allocateByLevel is a method that adjusts the game difficulty by setting the number of monsters in each level according to the game level.
  
  The set number of monsters is stored in allocateByLevel arraylist.

#### boolean overButton
  
  overButton is a method that returns true if the mouse position is over the button, false otherwise.

#### void drawbutton
  
  drawbutton is a method for drawing a button.

#### void mousepressed
  
  In the mousePressed method, when the button is clicked, the level value is received and stored, and the game setting is performed according to the level value.

  Adjust the difficulty according to the level with allocateByLevel(level), and execute popmonster with an arbitrary rail. popheart also runs on arbitrary rails.
  
  Also, it checks the start time.

#### int checkTimeInterval
  This method is to calculate the playing time. It takes the start time and the end time as parameters and calculate the interval. Then return the interval.

## Libraries
I imported two libraries.

#### java.util.*;

  This was imported to use an arraylist.

#### ddf.minim.*;

  'Minim' was imported to add background music and sound effects.


#### java.time.*
  This was imported to get time interval.

#### processing_websockets

## Source
I referenced the [Button](https://processing.org/examples/button.html) code on the Examples page of processing. 