/*
  Project 1
  Name of Project: typing game
  Author: Giyun Lee
  Date:
  ws://localhost:8025/typingGame
*/
import websockets.*;
import java.util.*;
import ddf.minim.*;
import java.time.*;

WebsocketServer ws;
Minim minim;
AudioPlayer bgm;
AudioPlayer getheart;
AudioPlayer getscore;
AudioPlayer dead;

ArrayList<Integer> allocateByLevel = new ArrayList();
ArrayList<monster> showedMonsters = new ArrayList();

ArrayList<heart> showedHearts = new ArrayList();
ArrayList<heart> showedHeartsCopy = new ArrayList();

ArrayList<Boolean> end = new ArrayList();

int level = 0;
int set = 0;
int record = 0;
boolean done = false;

PImage player;

int score;

int smallheart = 3;
int appearanceDelay = 5000;
float monsterSpeed;
String settingTitle = "";
int numOfMonsters;

String whatUserTyping = "";
String whatUserTyped = "";

popMonster popmonster0;
popMonster popmonster1;
popMonster popmonster2;
popHeart popheart;

int startS, startM, startH, endS, endM, endH;
JSONArray json = new JSONArray();
JSONObject info = new JSONObject();
JSONObject gs = new JSONObject();
JSONArray gamesetting = new JSONArray();

void setup()
{
  size(800,600);
  // load info : ranking and game setting
  json = loadJSONArray("data/ranking.json");
  gamesetting = loadJSONArray("data/gamesetting.json");
  gs = gamesetting.getJSONObject(gamesetting.size()-1);
  
  //websocket
  ws = new WebsocketServer(this,8080,"/typingGame");

  //audio
  minim = new Minim(this);
  bgm = minim.loadFile("Valley_of_Spies.mp3");
  getheart = minim.loadFile("heart.mp3");
  getscore = minim.loadFile("score.wav");
  dead = minim.loadFile("heartbreak.wav");
  bgm.play();
  bgm.setGain(-20);  
}

void draw()
{
  //drawing background
  background(152,110,70);
  
  //drawing button
  if(level == 0){ drawbutton(); }
  
  if(level != 0)
  {    
    //typing box
    drawtypingbox();
  
    //show score
    showscore();
    
    //show typing
    showtyping();
  
    //drawing monsters
    drawmonster();
    
    //drawing hearts
    drawheart();
    
    //draw hearts
    drawSmallHearts();
    
    //draw player
    drawplayer();
  }
}

void keyTyped () {
  if(key != ' ' && key != ENTER && key != BACKSPACE)
  {
    whatUserTyping = whatUserTyping + key;
  }
  if(key == BACKSPACE)
  {
    if(whatUserTyping.length()>0)
    {
      whatUserTyping = whatUserTyping.substring(0,whatUserTyping.length()-1);
    }
  }
  if(key == ' ' || key == ENTER)
  {
    whatUserTyped = whatUserTyping;
    whatUserTyping = "";
  }
}

void showscore()
{
  textSize(20);
  fill(0);
  textAlign(LEFT);
  text("my score : " + str(score),10,50);
}

void showtyping()
{
  textSize(20);
  fill(0);
  textAlign(LEFT);
  text(whatUserTyping,width/2-80,height-22);
  if(textWidth(whatUserTyping)>150)
  whatUserTyping = whatUserTyping.substring(1);
}

void drawtypingbox()
{
  rectMode(CENTER); 
  fill(10,41,59);
  noStroke();
  rect(width/2,10,800,20);
  
  fill(10,41,59);
  noStroke();
  rect(width/2,height-30,800,60);
  fill(255);
  stroke(10,41,59);
  rect(width/2, height-30,200,40);
}

void drawplayer()
{
  player = loadImage("player.png");
  image(player,0,height/5,165,403);
  
  if(showedMonsters.isEmpty() && end.size() == 3)
  { 
    rectMode(CENTER);
    fill(10,41,59);
    rect(width/2,height/2,200,160,5);
      
    textSize(30);
    fill(255);
    textAlign(CENTER);
    text("THE END",width/2,height/2-20);
    textSize(20);
    text("your score is " + str(score),width/2,height/2+25);  
    
    // check end time
    if(record == 0)
    {
      endS = second();
      endM = minute();
      endH = hour();
      
      print(checkTimeInterval(startS,startM, startH, endS, endM, endH));
      //record playtime and score
      info.setString("settingTitle", gs.getString("settingTitle"));
      info.setString("win", "win");
      info.setInt("level",level);
      info.setInt("score", score);
      info.setInt("playtime", checkTimeInterval(startS,startM, startH, endS, endM, endH));
      
      json.setJSONObject(json.size(),info);  
      saveJSONArray(json,"data/ranking.json");      
      
      done = true;
      record ++;
    }
  }
}

void drawmonster()
{
  ArrayList<monster> showedMonstersCopy = new ArrayList();
  showedMonstersCopy.addAll(showedMonsters);
  
  if(smallheart<0)
  {    
    return;
  }
  
  for(monster m : showedMonstersCopy)
  {
     m.draw();
  
    if(m.checking(whatUserTyped))
    {
      score += 10*m.level;
      whatUserTyped = "";
      showedMonsters.remove(m);
      getscore.play();
      getscore.setGain(-20);
      getscore.rewind();
    }
    
    if(m.x<155)
    {
      smallheart --;
      showedMonsters.remove(m);
      dead.play();
      dead.setGain(-10);
      dead.rewind();
    }
  }
}

void drawheart()
{
  ArrayList<heart> showedHeartsCopy = new ArrayList();
  showedHeartsCopy.addAll(showedHearts);
  
  if(smallheart<0)
  {
    rectMode(CENTER);
    fill(10,41,59);
    rect(width/2,height/2,200,160,5);
    
    textSize(30);
    fill(255);
    textAlign(CENTER);
    text("GAME OVER",width/2,height/2-20);
    textSize(20);
    text("your score is " + str(score),width/2,height/2+25);
    
    // check end time
    if(record == 0)
    {
      endS = second();
      endM = minute();
      endH = hour();
      
      print(checkTimeInterval(startS,startM, startH, endS, endM, endH));
      
      info.setString("settingTitle", gs.getString("settingTitle"));
      info.setString("win", "game over");
      info.setInt("level",level);
      info.setInt("score", score);
      info.setInt("playtime", checkTimeInterval(startS,startM, startH, endS, endM, endH));
    
      json.setJSONObject(json.size(),info);
      saveJSONArray(json,"data/ranking.json");

      done = true;
      record ++;
    }
    return;
  }
  
  if(showedMonsters.isEmpty() && end.size() == 3)
  {
    popheart.interrupt();
    return;
  }
  
  for(heart h : showedHeartsCopy)
  {
     h.draw();
  
    if(h.checking(whatUserTyped))
    {
      whatUserTyped = "";
      showedHearts.remove(h);
      smallheart++;
      getheart.play();
      getheart.setGain(-10);
      getheart.rewind();
    }
    
    if(h.x<155)
    {
      showedHearts.remove(h);
    }
  }
}

void drawSmallHearts()
{
  PImage heartpicture = loadImage("small_heart.png");
  for(int i=0;i<smallheart;i++)
  image(heartpicture,10+35*i,60,30,29);
}

void allocateByLevel(int level)
{
  int lv1, lv2, lv3;
  
  lv1 = 18-level;
  if(level%2 == 1)
  {
    lv2 = (level-1)/2+1;
    lv3 = (level-1)/2;
  }
  else lv2 = lv3 = level/2;
  
  for(int i=0; i<lv1; i++) allocateByLevel.add(1);
  for(int i=0; i<lv2; i++) allocateByLevel.add(2);
  for(int i=0; i<lv3; i++) allocateByLevel.add(3);
}

boolean overButton(int x, int y, int w, int h)  {
  if (mouseX >= x && mouseX <= x+w && 
      mouseY >= y && mouseY <= y+h){ return true; }
  else { return false; }
}

void drawbutton()
{
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<4;j++)
    {
      fill(10,41,59);
      noStroke();
      rect(145+130*j,80+160*i,120,120,5);
      fill(255);
      textAlign(CENTER);
      textSize(50);
      text(str(i*4+j+1),205+130*j,155+160*i);
    }
  }
}

void mousePressed()
{
  if(overButton(145,80,120,120)) level = 1;
  if(overButton(275,80,120,120)) level = 2;
  if(overButton(405,80,120,120)) level = 3;
  if(overButton(535,80,120,120)) level = 4;
  
  if(overButton(145,240,120,120)) level = 5;
  if(overButton(275,240,120,120)) level = 6;
  if(overButton(405,240,120,120)) level = 7;
  if(overButton(535,240,120,120)) level = 8;
 
  if(overButton(145,400,120,120)) level = 9;
  if(overButton(275,400,120,120)) level = 10;
  if(overButton(405,400,120,120)) level = 11;
  if(overButton(535,400,120,120)) level = 12;


  //adjust game setting
  //settingTitle = gs.getString("settingTitle");

  while(set<1)
  {
    appearanceDelay = gs.getInt("appearanceDelay")-100*(level-1);
    monsterSpeed = gs.getFloat("monsterSpeed");
    //numOfMonsters = gs.getInt("numOfMonsters");
    smallheart = gs.getInt("numOfHearts");
    
  
    println(appearanceDelay);
    println(monsterSpeed);
  
    println(smallheart);
  
    // setting
    allocateByLevel(level); // level
    //suffle to allocate rail
    ArrayList<Integer> shuffle = new ArrayList();
    shuffle.add(0);
    shuffle.add(1);
    shuffle.add(2);
    for(int i=0;i<3;i++)
    {
      int index = int(random(shuffle.size()));
      popmonster0 = new popMonster(shuffle.get(index),i,gs.getInt("numOfMonsters")); // rail i
      shuffle.remove(index);
    }
    popheart = new popHeart(int(random(3)),gs.getInt("numOfHearts"));  //random rail
    
    // check start time
    startS = second();
    startM = minute();
    startH = hour();
    
    set++;
  }
}

int checkTimeInterval(int ss, int sm, int sh, int es, int em, int eh)
{
  LocalTime start = LocalTime.of(sh, sm, ss);
  LocalTime end = LocalTime.of(eh, em, es);
  
  Duration duration = Duration.between(start, end);
  int interval = int(duration.getSeconds());
  
  return interval;
}
