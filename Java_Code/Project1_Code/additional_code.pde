void webSocketServerEvent (String msg)
{
  if(msg.equals("ranking"))
  {
    JSONObject ranking = new JSONObject();
    ranking.setString("currentSetting",gs.getString("settingTitle"));
    ranking.setJSONArray("records", json);

    String message = ranking.toString();
    ws.sendMessage(message);
  } else
  {
    try
    {
      synchronized (gs)
      {
        gs = parseJSONObject(msg);
        gamesetting.setJSONObject(gamesetting.size(),gs);
        saveJSONArray(gamesetting,"data/gamesetting.json");
      }
    } catch (Exception e)
    {
      println("Invalid command");
    }
  }
}


class popMonster extends Thread
{
  int delay;
  int i;
  int col;
  int num;
  
  popMonster(int delay,int col, int num)
  {
    this.delay = delay;
    i = 0;
    start();
    this.col = col;
    this.num = num;
  }
  
  void run()
  {
    ArrayList<Integer> allocateByLevelCopy = new ArrayList();
    allocateByLevelCopy.addAll(allocateByLevel);
    while(i<num)
    {
      try
      {
        Thread.sleep(300+200*delay);
      }  catch(Exception e){}
      
      int index = int(random(allocateByLevelCopy.size()));
      showedMonsters.add(new monster(800,col,allocateByLevelCopy.get(index)));
      allocateByLevelCopy.remove(index);
      i++;
      
      try
      {
        Thread.sleep(appearanceDelay); // depend on gamelevel
      }  catch(Exception e){}
      
      if(i == num-1) end.add(true);     
    }  
  }
}

class popHeart extends Thread
{
  int i;
  int col;
  int num;
  
  popHeart(int col, int num)
  {
    i = 0;
    start();
    this.col = col;
    this.num = num;
  }
  
  void run()
  {
    while(i<num)
    {
      try
      {
        Thread.sleep(10000);   //depend on gamelevel
      }  catch(Exception e){}
      
      showedHearts.add(new heart(800,col));
      i++;
      
      try
      {
        Thread.sleep(10000);
      }  catch(Exception e){}
    
    }  
  }
}



abstract class target
{
  String name;
  PImage picture;
  float x,y;
  
  target(int row, int col)
  {   
    
    this.x = row;
    this.y = 100 + col*150;
  }
  
  abstract void namelist();
  
  boolean checking(String typing)
  {
    if(typing.equals(name))
      return true;
    else return false;
  }
  
  abstract void draw();
}

class monster extends target
{
  int level;
  
  monster(int row, int col, int level)
  {
    super(row, col);
    this.level = level;
    namelist();
  }
  
  void namelist()
  {
    if(level == 1)
    {
        if(gs.getString("wb").length() != 0)
        {
          String[] wordbank = split(gs.getString("wb"),' ');
          int index = int(random(wordbank.length));
          name = wordbank[index];
        } else
        {
          JSONObject defaultgs = gamesetting.getJSONObject(0);
          String[] wordbank = split(defaultgs.getString("wb"),' ');
          int index = int(random(wordbank.length));
          name = wordbank[index];
        }
        
      
    }
    if(level == 2)
    {
      
      if(gs.getString("wb2").length() != 0)
      {
        String[] wordbank = split(gs.getString("wb2"),' ');
        int index = int(random(wordbank.length));
        name = wordbank[index];
      } else
      {
        JSONObject defaultgs = gamesetting.getJSONObject(0);
        String[] wordbank = split(defaultgs.getString("wb2"),' ');
        int index = int(random(wordbank.length));
        name = wordbank[index];
      }
    }
    if(level == 3)
    {
      if(gs.getString("wb3").length() != 0)
      {
        String[] wordbank = split(gs.getString("wb3"),' ');
        int index = int(random(wordbank.length));
        name = wordbank[index];
      } else
      {
        JSONObject defaultgs = gamesetting.getJSONObject(0);
        String[] wordbank = split(defaultgs.getString("wb3"),' ');
        int index = int(random(wordbank.length));
        name = wordbank[index];
      }
    }
  }
  
  void draw()
  {
    if(level == 1)
    {
      picture = loadImage("lv1.png");
      image(picture,x,y,112,102);
      x -= monsterSpeed;
    }
    if(level == 2)
    {
      picture = loadImage("lv2.png");
      image(picture,x,y-48,112,150);
      x -= monsterSpeed+0.2;
    }
    if(level == 3)
    {
      picture = loadImage("lv3.png");
      image(picture,x,y-49,112,151);
      x -= monsterSpeed+0.4;
    }
    
    if(textWidth(name)>110) textSize(16);
    else textSize(18);
    textAlign(CENTER);
    fill(0);
    text(name,x+56,y+97);
  }
}

class heart extends target
{ 
  heart(int row, int col)
  {
    super(row, col);
    namelist();
  }
  
  void namelist()
  {
    if(gs.getString("wbh").length() != 0)
    {
        String[] wordbank = split(gs.getString("wbh"),' ');
        int index = int(random(wordbank.length));
        name = wordbank[index];
    } else
    {
      JSONObject defaultgs = gamesetting.getJSONObject(0);
      String[] wordbank = split(defaultgs.getString("wbh"),' ');
      int index = int(random(wordbank.length));
      name = wordbank[index];
    }
  }
  
  void draw()
  {
    picture = loadImage("heart.png");
    image(picture,x,y,112,102);
    x -= monsterSpeed;
    
    if(textWidth(name)>110) textSize(16);
    else textSize(18);
    textAlign(CENTER);
    fill(0);
    text(name,x+56,y+97);
  }
}
