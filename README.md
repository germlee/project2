# Project 2 template #

This is the template for your project 2 submission. For a full description of the Project, please refer to [this document](https://docs.google.com/document/d/1jmFpRdEZJ_Cmk9Hp1q_I7Co9vRo-d5a3fUHcKBYSAPk/edit?usp=sharing).

## Personal information to add

* **Name**: 20170448
* **Student ID**: Giyun Lee
* **email**: rldbs9843@kaist.ac.kr

## Important notice before submitting

Make sure to create a branch with your studentID number before

---

## Table of content

* **Java_Code** source code (from Project 1): you can modify it
* **Javascript_Code** source code
* Demo **Video** (the actual video, not the link)
* **Description** of projects and notes in README.md (this file). 

## Description of the project

Please modify this file to include a description of your project.

You can add images here, as well as links. 
To format this document you will need to use Markdown language. Here a [tutorial](https://bitbucket.org/tutorials/markdowndemo).

Include the following things inside this file:

* briefly describe your project (objectives and what it does)
* describe how to use your project.
* list dependencies, if any (e.g., libraries you used)
* state sources of inspiration/code that you used or if anyone helped you
* Anything else you want to tell me

  
## Objective

This javascript part is for editing game setting of the typing game (Project 1) and checking the ranking.
You can change number of monsters or hearts, monster's speed and how fast the monsters show up. You can also change the words that are used in the typing game. Lastly, you can check the highest scores in the current game setting.

-------

## How to use this

It can be devided roughly in three parts : part for editing, for submiting and for ranking.

### Functions for editing

Before you start editing the game, you have to put a title on the new game setting. So type a title in the first typing box.
Then, you should type any words, or any texts you want in the second typing box. That is for changing the words used in the typing game. If you don't enter anything, then the words in the default setting will be used in the game.

Next, you can edit the setting about game operation with GUI. There are four sliders.
The slider named 'Interval' means the time intervals between the moments monsters show up from the right side. So if you decrease the interval value, then monsters show up fast, and if you increase it, then monsters do slowly.
The slider named 'monsterSpeed' make monster's movement fast and slow. The slider named 'numOfMonsters' change the total number of monsters that show up in the game. Lastly, the slider named 'numOfHearts' change the total number of hearts which stand for the lives of a player.

---

### Functions for submitting

There are three buttons : 'preview', 'submit', and 'initialization'. Those buttons are used after edit the game setting, to send the setting info to the typing game.

The preview button is to show the result of what you currently modify the setting. This show the information that described previously. Especially, you can see the result of the words extracted from the raw text that you typed.
The submit button is to send the game setting information to the typing game.
The intialization button is to initialize the setting after editing the setting. If you press this button, the game setting goes back to the default setting.

---

### Functions for ranking

There is a button named 'check ranking', and is to check the highest score of the current game setting.

To judge which record is the best, a length of playing time and a score is importan for the typing game. Also, it is impossible to compare the records of different levels.
So, if you press the 'check ranking' button, the browser shows the records of the highest score and the shortest playing time, for each level.

---

## important functions in code

### function preview()

This function is called when the botton named 'preview' is pressed.
This function do two big roles.
Fisrt, it refines a raw text. If you type any text (or you can just copy and paste a long text), then this funnction split the raw text by tokens and make all words to be lowercase. Then delete duplicated words and words that is too short. Then classify the words by difficaulty for typing. 
Second, it save all info about the game setting incluing title, words, and options of GUI. Then make the info into string to show on the browser, and show it on the screen.

### function submit()

This function is called when the button named 'submit' is pressed.
This function is to submit the game setting info. After the function preview is called, which can save what you typed and edited, this function stringify the json type info and send it to the server side. 

### function checkOverlap(arr)

This function get array as a parameter, and delete elements which is duplicated. Then return new array that has every kind of elements from original array.

### function getRankingInfo()

This function is to send a signal to the server side, which asks the server to send back the player records. The player records will be used to figure out the best records.

### function ranking(message)

This function is to extract the best records from the info that the server sends.
First, it extracts the records of the current game setting. (currentSetting
Next, it extracts only the winner's records.(winner)
Then, check the kinds of level that records include, because the highest scores will be extracted matching with each level.(level)
Then, filter the best records for each level. (bestRecords)
Lastly, show the result on the screen.(showRanking, finalstr)

### function initialization()

This function is to initailize the game setting, so it send the default setting to the server.

---
## Library

p5.gui

---

## Code inspiration

1. P5JS Example of Software Prototyping Class
   
   The example code <p5.gui> of Week 13.
2. Websockets examples
   
   The websockets example codes of software prototyping class.